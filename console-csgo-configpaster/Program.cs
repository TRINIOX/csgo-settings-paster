﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace console_csgo_configpaster
{
    public struct Globals
    {
        public static string config_dir = "config.ini";
        public static string cfg_dir;
        public static string root_dir;
    }

    class Program
    {
        [DllImport("user32.dll")] // https://stackoverflow.com/questions/22053112/maximizing-console-window-c-sharp/22053200#22053200
        public static extern bool ShowWindow(System.IntPtr hWnd, int cmdShow);

        private static void Maximize()
        {
            Process p = Process.GetCurrentProcess();
            ShowWindow(p.MainWindowHandle, 3); //SW_MAXIMIZE = 3
        }

        static void Main(string[] args)
        {
            Console.Title = "CSGO - Auto Settings Paster";
            Maximize();

            if (!File.Exists(Globals.config_dir))
            {
                Console.WriteLine($"{Globals.config_dir} not found");
                File.Create("config.ini").Dispose();

                Console.WriteLine($"{Globals.config_dir} created");

                IniFile _ini = new IniFile(Globals.config_dir);

                Console.WriteLine("Enter Steam Userdata Path");
                string _userdata_dir = Console.ReadLine();

                Console.WriteLine("Enter Config Path");
                string _cfg_dir = Console.ReadLine();

                _ini.Write("userdata", _userdata_dir, "General");
                _ini.Write("cfg", _cfg_dir, "General");
            }

            IniFile ini = new IniFile(Globals.config_dir);

            Globals.root_dir = $"{ini.Read("userdata", "General")}\\";
            Globals.cfg_dir = $"{ini.Read("cfg", "General")}\\";

            string latest_dir = GetLatestDir(Globals.root_dir);

            CopyFilesFromDirectory(Globals.cfg_dir, latest_dir);

            Console.ReadKey(); // Press any Key to exit
        }

        static string GetLatestDir(string root_dir)
        {
            string most_recent_dir = null;
            DateTime more_recent = new DateTime(1999, 1, 1, 00, 00, 00);

            DirectoryInfo di = new DirectoryInfo(root_dir);

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                if (dir.LastWriteTime > more_recent)
                {
                    most_recent_dir = dir.FullName;
                    more_recent = dir.CreationTime; // else it will replace it with anything newer than 1999 -> wrong result
                }
            }

            return most_recent_dir;
        }

        static void CopyFilesFromDirectory(string from, string to)
        {
            using (StreamWriter sw = new StreamWriter("log.txt"))
            {
                foreach (string dir in Directory.GetDirectories(from, "*", SearchOption.AllDirectories))
                {
                    string nDir = $"{dir.Replace(from, $"{to}\\")}";

                    string _msg = $"{DateTime.Now} - {nDir}";

                    Directory.CreateDirectory(nDir);
                    Console.WriteLine(_msg);

                    foreach (string file in Directory.GetFiles(dir))
                    {
                        FileInfo fi = new FileInfo(file);

                        string fileName = fi.Name;
                        string nFile = $"{nDir}\\{fileName}";

                        try
                        {
                            File.Copy(file, $"{nDir}\\{fileName}");

                            string msg = $"{DateTime.Now} - {file} copied to {nFile}";

                            Console.WriteLine($"{DateTime.Now} - " + $"{file} copied to {nFile}");
                            sw.WriteLine($"{DateTime.Now} - " + $"{file} copied to {nFile}");
                        }
                        catch
                        {
                            string msg = $"{DateTime.Now} - couldn't copy {file} to {nFile}";

                            Console.WriteLine(msg);
                            sw.WriteLine(msg);

                            continue;
                        }
                    }
                }
            }
        }
    }
}
